package com.example.ttbackendcalls;

import com.example.ttbackendcalls.utils.ApiUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class RetrofitClient {

    private static Retrofit retrofit = null;


    public static Retrofit getClient(String baseUrl) {

        if (retrofit == null) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        if(baseUrl.equals(ApiUtils.BASE_URL))
            return retrofit(okhttpBuilder(Constants.token), baseUrl);
        else
            return retrofit(okhttpBuilderC8Y(Constants.userT, Constants.passT), baseUrl);
    }

    public static OkHttpClient.Builder okhttpBuilder(String token) {
        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();

        final String credentials = "Bearer "+ token;//Credentials.basic(username, password);


        okhttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request authenticatedRequest = request.newBuilder()
                        .header("Authorization", credentials)
                        .header("Accept", "application/json")
                        .build();
                return chain.proceed(authenticatedRequest);
            }
        });

        return okhttpBuilder;
    }

    public static OkHttpClient.Builder okhttpBuilderC8Y(String username, String password) {
        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();

        final String credentials = Credentials.basic(username, password);


        okhttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request authenticatedRequest = request.newBuilder()
                        .header("Authorization", credentials)
                        .header("Accept", "application/json")
                        .build();
                return chain.proceed(authenticatedRequest);
            }
        });

        return okhttpBuilder;
    }

    public static Retrofit retrofit(OkHttpClient.Builder okhttpBuilder, String baseUrl) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.setLenient().create();
        return retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttpBuilder.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }


}