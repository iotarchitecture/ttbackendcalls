package com.example.ttbackendcalls.objects;

import com.example.ttbackendcalls.objects.AddressObject;
import com.example.ttbackendcalls.objects.AlertObject;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class AssetObject implements Serializable {
    private String assetName;
    private String assetType;
    private String assetId;
    private String c8yTrackerReference;
    private AddressObject origin;
    private String c8yGroupReference;
    private String c8yMessageBoxReference;
    private String tenantID;
    private String description;
    private String createDate;
    private String createUser;
    private String updateDate;
    private String updateUser;
    private transient JSONObject meta;
    private String assetStatus;
    private boolean isTrackerAttached;
    private String currentLat;
    private String currentLong;
    private ArrayList<AlertObject> alerts;
    private boolean isAlarmActive1;
    private transient String []  arrayMenu = new String[]{"Edit", "Share", "Delete"};
    private double batteryLevel = -1;
    private String groupName;
    private String groupId;
    private String trackerSerialNo;


    public String getTrackerSerialNo() {
        return trackerSerialNo;
    }

    public void setTrackerSerialNo(String trackerSerialNo) {
        this.trackerSerialNo = trackerSerialNo;
    }
    public double getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setArrayManu(String [] arrayMenu) {
        this.arrayMenu = arrayMenu;
    }
    public String [] getArrayMenu() {
        return arrayMenu ;
    }

    public void setAlarmActive(boolean x) {
        isAlarmActive1 =x;
    }

    public boolean getAlarmActive() {
       return isAlarmActive1 ;
    }

    public ArrayList<AlertObject> getAlert() {
        return alerts;
    }


    public void setAlert(ArrayList<AlertObject> alerts) {
        this.alerts = alerts;
    }

    public String getC8yGroupReference() {
        return c8yGroupReference;
    }


    public void setC8yGroupReference(String customerGroupReference) {
        this.c8yGroupReference = customerGroupReference;
    }

    public String getc8yMessageBoxReference() {
        return c8yMessageBoxReference;
    }


    public void setc8yMessageBoxReference(String companyGroupReference) {
        this.c8yMessageBoxReference = companyGroupReference;
    }

    public String getDescription() {
        return description;
    }


    public void setDescription(String description1) {
        this.description = description1;
    }


    public String getCreateDate() {
        return createDate;
    }


    public void setCreateDate(String createDate1) {
        this.createDate = createDate1;
    }

    public String getCreateUser() {
        return createUser;
    }


    public void setCreateUser(String createUser1) {
        this.createUser = createUser1;
    }


    public String getUpdateDate() {
        return updateDate;
    }


    public void setUpdateDate(String updateDate1) {
        this.updateDate = updateDate1;
    }

    public String getUpdateUser() {
        return updateUser;
    }


    public void setUpdateUser(String updateUser1) {
        this.updateUser = updateUser1;
    }

    public JSONObject getMeta() {
        return meta;
    }


    public void setMeta(JSONObject meta1) {
        this.meta = meta1;
    }

    public String getTenantID() {
        return tenantID;
    }


    public void setTenantID(String tenantID1) {
        this.tenantID = tenantID1;
    }

    public String getassetName() {
        return assetName;
    }

    public void setAssetName(String assetType) {
        this.assetName = assetType;
    }
    public String getassetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }
    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getTrackerId() {
        return c8yTrackerReference;
    }

    public void setTrackerId(String tracker) {
        this.c8yTrackerReference = tracker;
    }

    public AddressObject getOrigin() {
        return origin;
    }

    public void setOrigin(AddressObject origin) {
        this.origin = origin;
    }


    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public boolean isTrackerAttached() {
        return isTrackerAttached;
    }

    public void setTrackerAttached(boolean x) {
        isTrackerAttached = x;
    }

    public String getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(String currentLat) {
        this.currentLat = currentLat;
    }

    public String getCurrentLong() {
        return currentLong;
    }

    public void setCurrentLong(String currentLong) {
        this.currentLong = currentLong;
    }

    public AssetObject(String assetName, String assetType, String assetId, String c8yTrackerReference, AddressObject origin, String c8yGroupReference, String c8yMessageBoxReference, String tenantID, String description
            , String createDate, String createUser, String updateDate, String updateUser, JSONObject meta) {


        this.assetName = assetName;
        this.assetType = assetType;
        this.assetId = assetId;
        this.c8yTrackerReference = c8yTrackerReference;
        this.origin = origin;

        this.c8yGroupReference = c8yGroupReference;
        this.c8yMessageBoxReference = c8yMessageBoxReference;
        this.tenantID = tenantID;
        this.description = description;

        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
        this.meta = meta;
    }

    public AssetObject() {

    }
}