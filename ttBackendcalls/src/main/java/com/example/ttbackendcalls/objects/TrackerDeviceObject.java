package com.example.ttbackendcalls.objects;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class TrackerDeviceObject implements Serializable {
    private String deviceName;
    private String deviceType;
    private String deviceSerialNo;
    private String creationTime;
    private String lastUpdated;
    private String serialNo;
    private double batteryLevel = -1;

    public double getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    private String connectionStatus;
    private String availabilityStatus;

    private String [] arrayMenu = new String[]{"Edit", "Share", "Delete"};


    public void setArrayManu(String [] arrayMenu) {
        this.arrayMenu = arrayMenu;
    }
    public String [] getArrayMenu() {
        return arrayMenu ;
    }

    public String getCreationTime() {
        return creationTime;
    }
    public void setCreationTime(String creationTime){
        this.creationTime = creationTime;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }
    public void setLastUpdated(String lastUpdated){
        this.lastUpdated = lastUpdated;
    }

    public String getSerialNo() {
        return serialNo;
    }
    public void setSerialNo(String serialNo){
        this.serialNo = serialNo;
    }

    public String getDevicetName() {
        return deviceName;
    }

    public void setDeviceName(String assetType) {
        this.deviceName = assetType;
    }
    public String getdeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
    public String getDeviceSerialNo() {
        return deviceSerialNo;
    }

    public void setDeviceSerialNo(String deviceSerialNo) {
        this.deviceSerialNo = deviceSerialNo;
    }


    public TrackerDeviceObject(String deviceName, String deviceType, String deviceSerialNo) {


        this.deviceName = deviceName;
        this.deviceType = deviceType;
        this.deviceSerialNo = deviceSerialNo;

    }

    public TrackerDeviceObject() {

    }
}