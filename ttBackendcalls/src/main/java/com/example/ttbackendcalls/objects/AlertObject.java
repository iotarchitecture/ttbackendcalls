package com.example.ttbackendcalls.objects;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.List;

public class AlertObject implements Serializable {
    private String creationTime;
    private String severity;
    private String status;
    private String type;
    private String text;

    public String getCreationTime() {
        return creationTime;
    }
    public void setCreationTime(String creationTime){
        this.creationTime =creationTime;
    }

    public void setSeverity(String severity){
        this.severity =severity;
    }
    public String getSeverity() {
        return severity;
    }


    public void setStatus(String status){
        this.status =status;
    }
    public String getStatus() {
        return status;
    }


    public void setType(String type){
        this.type =type;
    }
    public String getType() {
        return type;
    }

    public void setAlertText(String text){
        this.text =text;
    }
    public String getAlertText() {
        return text;
    }


    public AlertObject(String creationTime, LatLng latlong) {
        this.creationTime = creationTime;

    }
    public AlertObject(){

    }
}