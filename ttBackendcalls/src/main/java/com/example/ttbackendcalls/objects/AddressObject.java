package com.example.ttbackendcalls.objects;



import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class AddressObject implements Serializable {
    private String name;
    private String adline1;
    private String adline2;
    private String city;
    private String state;
    private String country;
    private String pin;
    private String email;
    private String phone;
    private LatLng latLng;
    private String lat;
    private String lng;



    public AddressObject(String name, String adline1, String adline2, String city,String state, String country, String pin, String email,String phone) {
        this.name = name;
        this.adline1 = adline1;
        this.adline2 = adline2;
        this.city = city;
        this.state = state;
        this.country = country;
        this.pin = pin;
        this.email = email;
        this.phone = phone;
    }

    public AddressObject(JSONObject object) {
        try {
            if(object.has("name") && !object.get("name").equals(null))
                this.name =(String) object.get("name");
            if(object.has("address1") && !object.get("address1").equals(null))
                this.adline1 =(String) object.get("address1");
            if(object.has("address2") && !object.get("address2").equals(null)){
                this.adline2 = (String) object.get("address2");
            } else{
                this.adline2 = "";
            }
            if(object.has("city") && !object.get("city").equals(null))
                this.city = (String) object.get("city");
            if(object.has("state")  && !object.get("state").equals(null))
                this.state = (String) object.get("state");
            if(object.has("country") && !object.get("country").equals(null))
                this.country = (String) object.get("country");
            if(object.has("postalCode") && !object.get("postalCode").equals(null))
                this.pin = (String) object.get("postalCode");
            if(object.has("lat") && !object.get("lat").equals(null))
                this.lat = String.valueOf(object.getDouble("lat"));
            if(object.has("lng")  && !object.get("lng").equals(null))
                this.lng = String.valueOf(object.getDouble("lng"));
            if(object.has("email")  && object.get("email") != null)
                this.email =(String) object.get("email");
            if(object.has("phone")  && !object.get("phone").equals(null))
                this.phone = (String) object.get("phone");
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String getAddressString() {
        return adline1+" "+adline2+", "+city+", "+state+","+pin;
    }
    public String getName() {
        return name;
    }
    public String getAdline1() {
        return adline1;
    }
    public String getAdline2() {
        return adline2;
    }
    public String getCity() {
        return city;
    }
    public String getState() {
        return state;
    }
    public String getCountry() {
        return country;
    }
    public String getPin() {
        return pin;
    }
    public String getEmail() {
        return email;
    }
    public String getPhone() {
        return phone;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public String getLat() {
        return lat;
    }
    public String getLong() {
        return lng;
    }
    public void setAddressName(String name) {
        this.name = name;
    }
    public void setAdline1(String adLine1) {
        adline1 =adline1;
    }
    public void setAdline2(String adLine2) {
        adline2 =adline2;
    }

    public void setCity(String City) {
        city =City;
    }

    public void setState(String State) {
        state =State;
    }

    public void setCountry(String country1) { country =country1;
    }

    public void setPin(String Pin) {
        pin =Pin;
    }
    public void setEmail(String email1) {
        email =email1;
    }

    public void setPhone(String Phone) {
        phone =Phone;
    }

    public void setLatLng(LatLng LatLong) {
        latLng =LatLong;
    }

    public void setLat(String lat1) {
        lat =lat;
    }
    public void setLong(String long1) {
        lng = long1;
    }
}
