package com.example.ttbackendcalls.objects;

import android.location.Address;

import com.example.ttbackendcalls.objects.AddressObject;
import com.example.ttbackendcalls.objects.AlertObject;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ShipmentObject implements Serializable {
    private String journeyNo;
    private String shipmentName;
    private String shipmentId;
    private String c8yTrackerReference ;
    private String trackerSerialNo ;
    private AddressObject origin ;
    private AddressObject destination;
    private String targetDeliveredDate ;
    private String targetStartDate ;
    private String c8yCustomerGroupReference;
    private String c8yCompanyGroupReference ;
    private String tenantID ;
    private String description ;
    private String actualStartDate;
    private String actualDeliveredDate ;
    private String createDate ;
    private String createUser ;
    private String updateDate;
    private String updateUser;
    private String __typename ;
    private transient JSONObject meta ;
    private String shipmentStatus;
    private boolean isTrackerAttached;
    private String currentLat;
    private String currentLong;
    private ArrayList<AlertObject> alerts;
    private boolean isAlarmActive1;
    private transient String [] arrayMenu = new String[]{"Edit", "Share", "Delete"};

    public String getTrackerSerialNo() {
        return trackerSerialNo;
    }

    public void setTrackerSerialNo(String trackerSerialNo) {
        this.trackerSerialNo = trackerSerialNo;
    }

    public double getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    private double batteryLevel = -1;

    public void setArrayManu(String [] arrayMenu) {
        this.arrayMenu = arrayMenu;
    }
    public String [] getArrayMenu() {
        return arrayMenu ;
    }

    public void setAlarmActive(boolean x) {
        isAlarmActive1 =x;
    }

    public boolean getAlarmActive() {
        return isAlarmActive1 ;
    }

    public ArrayList<AlertObject> getAlert() {
        return alerts;
    }


    public void setAlert(ArrayList<AlertObject> alerts) {
        this.alerts = alerts;
    }

    public String getJourneyNo() {
        return journeyNo;
    }

    public void setJourneyNo(String journeyNo) {
        this.journeyNo = journeyNo;
    }

    public String getC8yCustomerGroupReference() {
        return c8yCustomerGroupReference;
    }


    public void setC8yCustomerGroupReference(String customerGroupReference) {
        this.c8yCustomerGroupReference = customerGroupReference;
    }

    public String getC8yCompanyGroupReference() {
        return c8yCompanyGroupReference;
    }


    public void setC8yCompanyGroupReference(String companyGroupReference) {
        this.c8yCompanyGroupReference = companyGroupReference;
    }
    public String getDescription() {
        return description;
    }


    public void setDescription(String description1) {
        this.description = description1;
    }
    public String getActualStartDate() {
        return actualStartDate;
    }


    public void setActualStartDate(String actualStartDate1) {
        this.actualStartDate = actualStartDate1;
    }

    public String getActualDeliveredDate() {
        return actualDeliveredDate;
    }


    public void setActualDeliveredDate(String actualDeliveredDate1) {
        this.actualDeliveredDate = actualDeliveredDate1;
    }

    public String getCreateDate() {
        return createDate;
    }


    public void setCreateDate(String createDate1) {
        this.createDate = createDate1;
    }

    public String getCreateUser() {
        return createUser;
    }


    public void setCreateUser(String createUser1) {
        this.createUser = createUser1;
    }


    public String getUpdateDate() {
        return updateDate;
    }


    public void setUpdateDate(String updateDate1) {
        this.updateDate = updateDate1;
    }
    public String getUpdateUser() {
        return updateUser;
    }


    public void setUpdateUser(String updateUser1) {
        this.updateUser = updateUser1;
    }

    public String get__typename() {
        return __typename;
    }


    public void set__typename(String __typename1) {
        this.__typename = __typename1;
    }

    public JSONObject getMeta() {
        return meta;
    }


    public void setMeta(JSONObject meta1) {
        this.meta = meta1;
    }

    public String getTenantID() {
        return tenantID;
    }


    public void setTenantID(String tenantID1) {
        this.tenantID = tenantID1;
    }
    public String getShipmentName() {
        return shipmentName;
    }

    public void setShipmentName(String shipmentName1) {
        this.shipmentName = shipmentName1;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId1) {
        this.shipmentId = shipmentId1;
    }

    public String getTrackerId() {
        return c8yTrackerReference;
    }

    public void setTrackerId(String tracker) {
        this.c8yTrackerReference = tracker;
    }

    public AddressObject getOrigin() {
        return origin;
    }

    public void setOrigin(AddressObject origin) {
        this.origin = origin;
    }

    public AddressObject getDestination() {
        return destination;
    }

    public void setDestination(AddressObject destination) {
        this.destination = destination;
    }

    public String getTargetDeliveredDate() {
        return targetDeliveredDate;
    }

    public void setTargetDeliveredDate(String deliveredDate) {
        this.targetDeliveredDate = deliveredDate;
    }

    public String getTargetStartDate() {
        return targetStartDate;
    }

    public void setTargetStartDate(String startDate) {
        this.targetStartDate =startDate;
    }

    public String getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus =shipmentStatus;
    }

    public boolean isTrackerAttached() {
        return isTrackerAttached;
    }
    public void setTrackerAttached(boolean x) {
        isTrackerAttached =x;
    }
    public String getCurrentLat() {
        return currentLat;
    }
    public void setCurrentLat(String currentLat) {
        this.currentLat = currentLat;
    }

    public String getCurrentLong() {
        return currentLong;
    }
    public void setCurrentLong(String currentLong) {
        this.currentLong = currentLong;
    }

    public ShipmentObject(String shipmentName, String shipmentId, String c8yTrackerReference, AddressObject origin, AddressObject destination,
                          String targetStartDate, String targetDeliveredDate, String c8yCustomerGroupReference, String c8yCompanyGroupReference, String tenantID, String description
    , String actualStartDate, String actualDeliveredDate, String createDate, String createUser, String updateDate, String updateUser, String __typename, JSONObject meta) {


        this.shipmentName =shipmentName;
        this.shipmentId =shipmentId;
        this.c8yTrackerReference = c8yTrackerReference;
        this.origin = origin;
        this.destination = destination;

        this.targetStartDate =targetStartDate;
        this.targetDeliveredDate=targetDeliveredDate;
        this.c8yCompanyGroupReference=c8yCompanyGroupReference;
        this.c8yCustomerGroupReference=c8yCustomerGroupReference;
        this.tenantID=tenantID;
        this.description=description;
        this.actualStartDate=actualStartDate;
        this.actualDeliveredDate=actualDeliveredDate;
        this.createUser=createUser;
        this.createDate=createDate;
        this.updateUser=updateUser;
        this.updateDate=updateDate;
        this.__typename=__typename;
        this.meta=meta;
    }

    public ShipmentObject(){

    }







}