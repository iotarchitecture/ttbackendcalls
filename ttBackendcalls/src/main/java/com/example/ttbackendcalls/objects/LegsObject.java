package com.example.ttbackendcalls.objects;

import com.example.ttbackendcalls.objects.DistanceObject;
import com.example.ttbackendcalls.objects.DurationObject;
import com.example.ttbackendcalls.objects.StepsObject;

import java.util.List;

public class LegsObject {



    private DistanceObject distance;

    private DurationObject duration;
    private List<StepsObject> steps;

    public LegsObject(DurationObject duration, DistanceObject distance, List<StepsObject> steps) {

        this.steps = steps;
        this.duration = duration;
        this.distance = distance;
    }

    public List<StepsObject> getSteps() {
        return steps;
    }

    public DistanceObject getDistance() {
        return distance;
    }

    public DurationObject getDuration() {
        return duration;
    }
}