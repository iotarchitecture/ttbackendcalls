package com.example.ttbackendcalls.objects;

import android.location.Address;

import com.example.ttbackendcalls.objects.AddressObject;

import org.json.JSONObject;

import java.io.Serializable;

public class GroupObject implements Serializable {

    private String groupId;
    private String groupName;
    private String groupType;
    private AddressObject groupAddress;
    private String companyName;
    private String adline1;
    private String adline2;
    private String cityStateZip;



    public String getGroupId() {
        return groupId;
    }


    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public AddressObject getGroupAddress() {
        return groupAddress;
    }

    public void setGroupAddress(AddressObject groupAddress1) {
        this.groupAddress = groupAddress1;
    }


    public String getAddressLine1() {
        return adline1;
    }

    public void setAddressLine1(String adline1) {
        this.adline1 = adline1;
    }

    public String getAddressLine2() {
        return adline2;
    }

    public void setAddressLine2(String adline2) {
        this.adline2 = adline2;
    }

    public String getCityStateZip() {
        return cityStateZip;
    }

    public void setCityStateZip(String cityStateZip) {
        this.cityStateZip = cityStateZip;
    }


    public GroupObject(String companyName, String groupId, String groupName, String groupType, AddressObject groupAddress, String adline1, String adline2, String cityStateZip) {

        this.companyName = companyName;
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupType = groupType;
        this.groupAddress= groupAddress;
        this.adline1 = adline1;
        this.adline2 = adline2;
        this.cityStateZip = cityStateZip;

    }

    public GroupObject() {

    }
}