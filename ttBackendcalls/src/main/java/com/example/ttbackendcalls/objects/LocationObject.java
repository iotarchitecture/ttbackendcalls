package com.example.ttbackendcalls.objects;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;


public class LocationObject implements Serializable {
    private String creationTime;
    private LatLng latlong;


    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime){
        this.creationTime =creationTime;
    }

    public LatLng getLatlong() {
        return latlong;
    }

    public void setLatlong(LatLng latlong){
        this.latlong =latlong;
    }

    public LocationObject(String creationTime, LatLng latlong) {
        this.creationTime = creationTime;
        this.latlong = latlong;
    }
    public LocationObject(){

    }
}