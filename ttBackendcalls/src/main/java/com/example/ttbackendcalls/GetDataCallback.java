package com.example.ttbackendcalls;


import org.json.JSONObject;

//Interface used to pass in data from adapter to new activity
public interface GetDataCallback {
    void onGetJsonObject(JSONObject dataObject);
    void onError();
}
