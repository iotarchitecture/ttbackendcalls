package com.example.ttbackendcalls.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.ttbackendcalls.GetDataCallback;
import com.example.ttbackendcalls.objects.AddressObject;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import static com.example.ttbackendcalls.apiCalls.CommonAPICalls.getDeviceIdFromSerial;

public class CommonFunctionsUtils {
    private CommonFunctionsUtils() {}


    public static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static double meterDistanceBetweenPoints(double lat1, double lng1, double lat2, double lng2) {
        double R = 6371000f; // Radius of the earth in m
        double dLat = (lat1 - lat2) * Math.PI / 180f;
        double dLon = (lng1 - lng2) * Math.PI / 180f;
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1 * Math.PI / 180f) * Math.cos(lat2 * Math.PI / 180f) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2f * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
        return d;
    }


    public static LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null || address.size() == 0) {
                return null;
            }
            if (!address.get(0).getCountryCode().equals("US")) {
                Toast.makeText(context, "The address is not valid",Toast.LENGTH_LONG);
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    public static boolean verifyAddress (Context context, AddressObject homeOriginAddress) {
        boolean isAddressValid = false;
        String originAddressStr = homeOriginAddress.getAddressString();
        LatLng originLatLong = getLocationFromAddress(context, originAddressStr);
        homeOriginAddress.setLatLng(originLatLong);
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses=null;
        try {
            if(originLatLong!=null) {
                addresses = geocoder.getFromLocation(originLatLong.latitude, originLatLong.longitude, 1);
            }
            if (addresses == null || addresses.size()==0) {
                isAddressValid = false;
                Toast.makeText(context, "The address is not valid, Please check the address you have entered! ", Toast.LENGTH_LONG);
            } else {
                isAddressValid = true;
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }
        return isAddressValid;
    }

    public static void tokenExpire(Context context){

    Toast.makeText(context, "Token Expired! Please Sign out and Sign in again.", Toast.LENGTH_LONG).show();

    }
    public static void notAcceptable(Context context){
        Toast.makeText(context, "Credentials Not Acceptable", Toast.LENGTH_LONG).show();
        //Intent myIntent = new Intent(context, activity.getClass());
        //context.startActivity(myIntent);

    }
    public static class TrackerID
    {
        public String c8yTrackerReference;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String fromISO8601UTC(String dateStr) {
        String formatDateTime;
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        final LocalDateTime parsed = LocalDateTime.parse(dateStr, formatter);
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss a").withZone(ZoneId.of("EST"));
        formatDateTime =parsed.format(formatter1);

        return formatDateTime;

    }
    public static class GroupID
    {
        public String c8yCustomerGroupReference;
    }
}
