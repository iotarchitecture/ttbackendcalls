package com.example.ttbackendcalls.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.ttbackendcalls.JsonPlaceHolderApi;
import com.example.ttbackendcalls.objects.AssetObject;
import com.example.ttbackendcalls.objects.AlertObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssetUtils {
    private AssetUtils() {}

    private static JsonPlaceHolderApi jsonPlaceHolderApi;


    private ArrayList<AlertObject> loadAlertsList(JSONObject allAlerts) {
        //  jdb= new assetDatabaseHelper(this);
        // assetArrayList = jdb.getAllJourney();
        JSONArray items;
        JSONObject alertItem;
        String creationTime;
        // progressBar.setVisibility(View.VISIBLE);
        ArrayList<AlertObject>  alertArrayList = new ArrayList<AlertObject>();
        try {

            items =allAlerts.getJSONArray("items");
            for(int i =0; i<items.length();i++){
                AlertObject alert=new AlertObject();
                alertItem =items.getJSONObject(i);
                if(!alertItem.get("severity").equals("null")){
                    alert.setSeverity(String.valueOf(alertItem.get("severity")));
                }
                if(!alertItem.get("creationTime").equals("null")){
                    alert.setCreationTime(String.valueOf(alertItem.get("creationTime")));
                }


                if(!alertItem.get("status").equals("null")){
                    alert.setStatus(String.valueOf(alertItem.get("status")));
                }
                if(!alertItem.get("type").equals("null")){
                    alert.setType(String.valueOf(alertItem.get("type")));
                }
                if(!alertItem.get("text").equals("null")){
                    alert.setAlertText(String.valueOf(alertItem.get("text")));
                }
             /*   if(!locationItem.get("c8y_Position").equals(null)) {
                    JSONObject c8y_Position =locationItem.getJSONObject("c8y_Position");
                    location.setLatlong( new LatLng(Double.valueOf(String.valueOf(c8y_Position.get("lat"))),Double.valueOf(String.valueOf(c8y_Position.get("long")))));
                }*/

                //Log.i("TabsActivity", "asset ID: "+String.valueOf(asset.getassetId()));
                alertArrayList.add(alert);
                //progressBar.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return alertArrayList;

    }



}
