package com.example.ttbackendcalls.utils;

import com.example.ttbackendcalls.JsonPlaceHolderApi;
import com.example.ttbackendcalls.RetrofitClient;


public class ApiUtils {
    private ApiUtils() {}

    public static String tenantID = "tracker-pilot/";

    public static final String C8Y_URL = "https://tracker-pilot.stage.sbdconnect.io/";
    //public static final String BASE_URL = "https://o0z4i1ipl1.execute-api.us-east-1.amazonaws.com/stage/"+tenantID;
    //public static final String BASE_URL = "https://stage.totaltraverse.com/api/"+tenantID;
    public static final String BASE_URL = "https://dev.totaltraverse.com/api/"+tenantID;


    public static JsonPlaceHolderApi getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(JsonPlaceHolderApi.class);
    }

}
