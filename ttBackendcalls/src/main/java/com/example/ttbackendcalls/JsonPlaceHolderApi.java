package com.example.ttbackendcalls;


import com.example.ttbackendcalls.apiCalls.CommonAPICalls;
import com.example.ttbackendcalls.utils.CommonFunctionsUtils;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;



public interface JsonPlaceHolderApi {
    //String id =TrackerSelectionActivity.shipmentId;
    //{{c8yurl}}/inventory/managedObjects?query=$filter=(c8y_Hardware.serialNumber eq '{{serial}}')&pageSize=1000

    //@GET("inventory/managedObjects?query=$filter=(c8y_Hardware.serialNumber eq '{serial}')")
    @GET("inventory/managedObjects")
    Call<String> getDeviceId(@Query("query") String query);

    @GET("inventory/managedObjects")
    Call<String> getAllTrackerDevices(@Query("pageSize") int pageSize, @Query("withTotalPages") boolean withTotalPages, @Query("fragmentType") String fragmentType, @Query("currentPage") int currentPage, @Query("type") String deviceType );

    @GET("user/currentUser")
    Call<String> getuserName(@Query("userName") String userName);
    @GET("/event/events?source=403&pageSize=1&revert=true")
    Call<String> getEvents();
    @GET("/measurement/measurements?source=403&pageSize=1&revert=true")
    Call<String> getMeasurements();

    //LOGIN

    @POST("user/login")
    Call<String> loginUser(@Body CommonAPICalls.Credentials credentials);

     //SHIPMENT
     @POST("inventory/shipments")
     Call<String> createShipmentMinimal(@Header("Authorization") String token, @Body RequestBody object);

    @POST("inventory/shipments")
    Call<String> createShipmentFull(@Header("Authorization") String token, @Body RequestBody object);

    @PUT("inventory/shipments/{id}")
    Call<String> updateShipmentMinimal(@Header("Authorization") String token, @Path("id") String id, @Body RequestBody object);

    @PUT("inventory/shipments/{id}")
    Call<String> updateShipmentFull(@Header("Authorization") String token, @Path("id") String id, @Body RequestBody object);

    @GET("inventory/shipments/")
    Call<String> getAllShipments(@Header("Authorization") String token, @Query("pageSize") int pageSize, @Query("withTotalPages") boolean withTotalPages, @Query("pageNum") int currentPage);

    @GET("inventory/shipments/{id}")
    Call<String> getShipmentWithId(@Header("Authorization") String token, @Path("id") String id);

    @PUT("inventory/shipments/{id}/device")
    Call<String> attachTrackerWithShipmentId(@Header("Authorization") String token, @Path("id") String id, @Body CommonFunctionsUtils.TrackerID trackerID);

    @GET("inventory/shipments/{id}/locations")
    Call<String> getLocationWithShipmentId(@Header("Authorization") String token, @Path("id") String id, @Query("pageSize") int pageSize, @Query("withTotalPages") boolean withTotalPages, @Query("pageNum") int currentPage);

    @POST("inventory/shipments/{id}/locations")
    Call<String> setLocationWithShipmentId(@Header("Authorization") String token, @Path("id") String id, @Body RequestBody object);

    @DELETE("inventory/shipments/{id}/device")
    Call<String> detachTrackerWithShipmentId(@Header("Authorization") String token, @Path("id") String id);

    @DELETE("inventory/shipments/{id}")
    Call<String> deleteShipmentWithId(@Header("Authorization") String token, @Path("id") String id);

    //ASSETS

    @POST("inventory/assets")
    Call<String> createAssetMinimal(@Header("Authorization") String token, @Body RequestBody object);

    @POST("inventory/assets")
    Call<String> createAssetFull(@Header("Authorization") String token, @Body RequestBody object);

    @PUT("inventory/assets/{id}")
    Call<String> updateAssetMinimal(@Header("Authorization") String token, @Path("id") String id, @Body RequestBody object);

    @PUT("inventory/assets/{id}")
    Call<String> updateAssetFull(@Header("Authorization") String token, @Path("id") String id, @Body RequestBody object);

    @PUT("inventory/assets/{id}")
    Call<String> addCustomer(@Header("Authorization") String token, @Path("id") String id, @Body CommonFunctionsUtils.GroupID groupID);

    @GET("inventory/assets/")
    Call<String> getAllAssets(@Header("Authorization") String token, @Query("pageSize") int pageSize, @Query("withTotalPages") boolean withTotalPages, @Query("pageNum") int currentPage);

    @GET("inventory/assets/{id}")
    Call<String> getAssetWithId(@Header("Authorization") String token, @Path("id") String id);

    @PUT("inventory/assets/{id}/device")
    Call<String> attachTrackerWithAssetId(@Header("Authorization") String token, @Path("id") String id, @Body CommonFunctionsUtils.TrackerID trackerID);

    @GET("inventory/assets/{id}/locations")
    Call<String> getLocationWithAssetId(@Header("Authorization") String token, @Path("id") String id, @Query("pageSize") int pageSize, @Query("withTotalPages") boolean withTotalPages, @Query("pageNum") int currentPage);

    @POST("inventory/assets/{id}/locations")
    Call<String> setLocationWithAssetId(@Header("Authorization") String token, @Path("id") String id, @Body RequestBody object);

    @DELETE("inventory/assets/{id}/device")
    Call<String> detachTrackerWithAssetId(@Header("Authorization") String token, @Path("id") String id);

    @DELETE("inventory/assets/{id}")
    Call<String> deleteAssetWithId(@Header("Authorization") String token, @Path("id") String id);

    //GROUPS

    @GET("groups?pageSize=2000&pageNum=0")
    Call<String> getAllGroups(@Header("Authorization") String token);

    @GET("groups/{id}")
    Call<String> getGroupDetail(@Header("Authorization") String token, @Path("id") String id);

    //ALARMS
    @GET("inventory/shipments/{id}/alerts")
    Call<String> getAlertsWithShipmentId(@Header("Authorization") String token, @Path("id") String id);

    @GET("inventory/assets/{id}/alerts")
    Call<String> getAlertsWithAssetId(@Header("Authorization") String token, @Path("id") String id);
}

