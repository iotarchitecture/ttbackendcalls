package com.example.ttbackendcalls.apiCalls;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.ttbackendcalls.Constants;
import com.example.ttbackendcalls.GetDataCallback;
import com.example.ttbackendcalls.JsonPlaceHolderApi;
import com.example.ttbackendcalls.RetrofitClient;
import com.example.ttbackendcalls.utils.CommonFunctionsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.ttbackendcalls.Constants.token;
import static com.example.ttbackendcalls.utils.ApiUtils.BASE_URL;
import static com.example.ttbackendcalls.utils.ApiUtils.C8Y_URL;
import static com.example.ttbackendcalls.utils.CommonFunctionsUtils.notAcceptable;
import static com.example.ttbackendcalls.utils.CommonFunctionsUtils.tokenExpire;


public class AssetAPICalls {

    public static void  getAssetWithId(final Context context, String assetId , final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getAssetWithId("Bearer "+ token, assetId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getAssetWithId   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        getDataCallback.onGetJsonObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void getAllAssets(final Context context,int currentPage,  final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getAllAssets("Bearer "+ token, Constants.pageSize, Constants.withTotalPages, currentPage);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getAllAssets   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else if (response.code() == 406){
                            notAcceptable(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();
                        return;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        JSONObject jsonObjectAssets = new JSONObject(response.body());
                        if (jsonObjectAssets != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAssets);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }

        });

    }


    public static void getLocationWithAssetId(final Context context, int currentPage, String assetId, final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getLocationWithAssetId("Bearer "+token, assetId, Constants.pageSizeLocation*(currentPage+1), Constants.withTotalPages, 0);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    //Toast.makeText(context, "Wrong Credentials  " + response.code(), Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getLocationWithAssetId   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectLocation = new JSONObject(response.body());
                        if (jsonObjectLocation != null) {
                            getDataCallback.onGetJsonObject(jsonObjectLocation);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });

    }

    public static void setLocationWithAssetId(final Context context, String assetId, RequestBody body, final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.setLocationWithAssetId("Bearer "+token, assetId, body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "setLocationWithAssetId   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        Toast.makeText(context, "Location is not set  " + response.code(), Toast.LENGTH_LONG).show();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectLocation = new JSONObject(response.body());
                        if (jsonObjectLocation != null) {
                            getDataCallback.onGetJsonObject(jsonObjectLocation);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });

    }

    public static void getAlertsWithAssetId(final Context context, String assetId, final GetDataCallback getDataCallback) {
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getAlertsWithAssetId("Bearer "+token, assetId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getAlertsWithAssetId  "+ jObjError.toString());
                        String res = jObjError.get("message").toString();

                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectAlerts = new JSONObject(response.body());
                        if (jsonObjectAlerts != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAlerts);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public static void createAssetFull(final Context context, JSONObject jsonRequestBody, final GetDataCallback getDataCallback) {
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Log.d("response", jsonRequestBody.toString());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonRequestBody.toString());
        Call<String> call = jsonPlaceHolderApi.createAssetFull("Bearer "+token, body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Asset not Created  " + response.code(), Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "createAssetFull   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectAlerts = new JSONObject(response.body());
                        if (jsonObjectAlerts != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAlerts);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public static void updateAssetFull(final Context context, String assetId, JSONObject jsonRequestBody, final GetDataCallback getDataCallback) {
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Log.d("response", jsonRequestBody.toString());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonRequestBody.toString());
        Call<String> call = jsonPlaceHolderApi.updateAssetFull("Bearer "+token, assetId, body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Asset not Updated  " + response.code(), Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "updateAssetFull   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectAlerts = new JSONObject(response.body());
                        if (jsonObjectAlerts != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAlerts);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }
    public static void attachTrackerWithAssetId(final Context context, String assetId, CommonFunctionsUtils.TrackerID trackerId, final GetDataCallback getDataCallback) {
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<String> call = jsonPlaceHolderApi.attachTrackerWithAssetId("Bearer "+token, assetId, trackerId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "attachTrackerWithAssetId   "+ jObjError.toString());
                        Toast.makeText(context, "Tracker not attached:  " + jObjError.toString(), Toast.LENGTH_LONG).show();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }
    public static void detachTrackerWithAssetId(final Context context, String assetId, final GetDataCallback getDataCallback) {

        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<String> call = jsonPlaceHolderApi.detachTrackerWithShipmentId("Bearer "+token, assetId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "detachTrackerWithAssetId   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }



    public static void addCustomerGroup(final Context context, String assetId, CommonFunctionsUtils.GroupID groupID, final GetDataCallback getDataCallback) {
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.addCustomer("Bearer "+token, assetId, groupID);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "addCustomerGroup   "+ jObjError.toString());
                        Toast.makeText(context, "Add user failed:  " + jObjError.toString(), Toast.LENGTH_LONG).show();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectAlerts = new JSONObject(response.body());
                        if (jsonObjectAlerts != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAlerts);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }


    public static void deleteAssetWithAssetId(final Context context, String assetId, final GetDataCallback getDataCallback) {

        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<String> call = jsonPlaceHolderApi.deleteAssetWithId("Bearer "+token, assetId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "deleteAssetWithAssetId   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

}
