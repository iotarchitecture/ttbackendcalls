package com.example.ttbackendcalls.apiCalls;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;


import com.example.ttbackendcalls.Constants;
import com.example.ttbackendcalls.GetDataCallback;
import com.example.ttbackendcalls.JsonPlaceHolderApi;
import com.example.ttbackendcalls.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.ttbackendcalls.Constants.token;
import static com.example.ttbackendcalls.utils.ApiUtils.BASE_URL;
import static com.example.ttbackendcalls.utils.ApiUtils.C8Y_URL;
import static com.example.ttbackendcalls.utils.CommonFunctionsUtils.tokenExpire;

public class CommonAPICalls {

    public static void  authLogin(final Context context, String  userT, String passT, final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Credentials loginCredentials = new Credentials();
        loginCredentials.username = userT;
        loginCredentials.password = passT;

        Call<String> call = jsonPlaceHolderApi.loginUser(loginCredentials);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Wrong Credentials  " + response.code(), Toast.LENGTH_LONG).show();

                    getDataCallback.onError();
                    return;
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        getDataCallback.onGetJsonObject(jsonObject);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });

    }
    public static void  getDeviceIdFromSerial(String serialId, final GetDataCallback getDataCallback){
        String query =  "$filter=(c8y_Hardware.serialNumber eq '"+serialId+"')";
        Retrofit retrofit_C8Y = RetrofitClient.getClient(C8Y_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi_c8y = retrofit_C8Y.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi_c8y.getDeviceId(query);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getDeviceIdFromSerial   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        getDataCallback.onGetJsonObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                //Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void  getAllGroups(final Context context, final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getAllGroups("Bearer "+ token);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "getAllGroups   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {
                        JSONArray jsonArray = new JSONArray(response.body());
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("item", jsonArray);
                        if(jsonArray != null)
                            getDataCallback.onGetJsonObject(jsonObject);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }

        });

    }


    public static void  getGroupDetails(final Context context, String groupId, final GetDataCallback getDataCallback){
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getGroupDetail("Bearer "+ token, groupId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "getGroupDetails   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }

        });

    }


    public static void  getAllTrackerDevices(int currentPage, final GetDataCallback getDataCallback){
        Retrofit retrofit_C8Y = RetrofitClient.getClient(C8Y_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi_c8y = retrofit_C8Y.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi_c8y.getAllTrackerDevices(Constants.pageSize, Constants.withTotalPages, Constants.fragmentType, currentPage, Constants.deviceType);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getAllTrackerDevices   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        getDataCallback.onGetJsonObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                //Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public static class Credentials
    {
        public String username;
        public String password;
    }
}
