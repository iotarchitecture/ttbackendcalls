package com.example.ttbackendcalls.apiCalls;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.ttbackendcalls.Constants;
import com.example.ttbackendcalls.GetDataCallback;
import com.example.ttbackendcalls.JsonPlaceHolderApi;
import com.example.ttbackendcalls.RetrofitClient;
import com.example.ttbackendcalls.utils.CommonFunctionsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.ttbackendcalls.Constants.token;
import static com.example.ttbackendcalls.utils.ApiUtils.BASE_URL;
import static com.example.ttbackendcalls.utils.CommonFunctionsUtils.tokenExpire;

public class ShipmentAPICalls {

    public static void  getShipmentWithId(final Context context, String shipmentId , final GetDataCallback getDataCallback){
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getShipmentWithId("Bearer "+token, shipmentId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getShipmentWithId   "+ jObjError.toString());
                        String res = jObjError.get("message").toString();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                } else {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        getDataCallback.onGetJsonObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void getAllShipments(final Context context, int currentPage, final GetDataCallback getDataCallback){
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getAllShipments("Bearer "+ token, Constants.pageSize, Constants.withTotalPages, currentPage);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "getAllShipments   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();
                        return;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }

        });

    }


    public static void getLocationWithShipmentId(final Context context,int currentPage, String shipmentId, final GetDataCallback getDataCallback){
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getLocationWithShipmentId("Bearer "+token, shipmentId, Constants.pageSizeLocation, Constants.withTotalPages, currentPage);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "getLocationWithShipmentId   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectLocation = new JSONObject(response.body());
                        if (jsonObjectLocation != null) {
                            getDataCallback.onGetJsonObject(jsonObjectLocation);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

    }

    public static void setLocationWithShipmentId(final Context context, String shipmentId, RequestBody body, final GetDataCallback getDataCallback){
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.setLocationWithShipmentId("Bearer "+token, shipmentId, body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "setLocationWithShipmentId   "+ jObjError.toString());
                        Toast.makeText(context, "Location is not set  " + response.code(), Toast.LENGTH_LONG).show();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectLocation = new JSONObject(response.body());
                        if (jsonObjectLocation != null) {
                            getDataCallback.onGetJsonObject(jsonObjectLocation);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

    }

    public static void getAlertsWithShipmentId(final Context context, String shipmentId, final GetDataCallback getDataCallback) {
        Retrofit retrofit = RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.getAlertsWithShipmentId("Bearer "+token, shipmentId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.d("response", "getAlertsWithShipmentId  "+ jObjError.toString());
                        String res = jObjError.get("message").toString();

                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectAlerts = new JSONObject(response.body());
                        if (jsonObjectAlerts != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAlerts);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public static void createShipmentFull(final Context context, JSONObject jsonRequestBody, final GetDataCallback getDataCallback) {
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Log.d("response", jsonRequestBody.toString());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonRequestBody.toString());
        Call<String> call = jsonPlaceHolderApi.createShipmentFull("Bearer "+token, body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Shipment not Created  " + response.code(), Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "createShipmentFull   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static void updateShipmentFull(final Context context, String shipmentId,JSONObject jsonRequestBody,  final GetDataCallback getDataCallback) {
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Log.d("response", jsonRequestBody.toString());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonRequestBody.toString());
        Call<String> call = jsonPlaceHolderApi.updateShipmentFull("Bearer "+token, shipmentId, body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Shipment not Updated  " + response.code(), Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "updateShipmentFull   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObjectAlerts = new JSONObject(response.body());
                        if (jsonObjectAlerts != null) {
                            getDataCallback.onGetJsonObject(jsonObjectAlerts);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public static void attachTrackerWithShipmentId(final Context context, String shipmentId, CommonFunctionsUtils.TrackerID trackerId, final GetDataCallback getDataCallback) {
        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.attachTrackerWithShipmentId("Bearer "+token, shipmentId, trackerId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "attachTrackerWithShipmentId   "+ jObjError.toString());
                        Toast.makeText(context, "Tracker not attached:  " + jObjError.toString(), Toast.LENGTH_LONG).show();
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public static void detachTrackerWithShipmentId(final Context context, String shipmentId, final GetDataCallback getDataCallback) {

        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.detachTrackerWithShipmentId("Bearer "+token, shipmentId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "detachTrackerWithShipmentId   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static void deleteShipmentWithShipmentId(final Context context, String shipmentId, final GetDataCallback getDataCallback) {

        Retrofit retrofit= RetrofitClient.getClient(BASE_URL);
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<String> call = jsonPlaceHolderApi.deleteShipmentWithId("Bearer "+token, shipmentId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String res = jObjError.get("message").toString();
                        Log.d("response", "deleteShipmentWithShipmentId   "+ jObjError.toString());
                        if(response.code() == 500 && res.equals("null")) {
                            tokenExpire(context);
                        } else {
                            Toast.makeText(context, jObjError.toString(), Toast.LENGTH_LONG).show();
                        }
                        getDataCallback.onError();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;
                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject != null) {
                            getDataCallback.onGetJsonObject(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }


            } @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}

